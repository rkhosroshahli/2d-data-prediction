async function getData() {
    const carsDataReq = await fetch('https://storage.googleapis.com/tfjs-tutorials/carsData.json');
    const carsData = await carsDataReq.json();
    const filteredCars = carsData.map(car => ({
        mpg: car.Miles_per_Gallon,
        horsepower: car.Horsepower,
    }))
        .filter(car => (car.mpg != null && car.horsepower != null));

    return filteredCars;
}

function modelCreator() {
    const model = tf.sequential();

    model.add(tf.layers.dense({
        inputShape: [1], units: 1, useBias: true
    }));

    model.add(tf.layers.dense({ units: 10, activation: 'relu' }));
    model.add(tf.layers.dense({ units: 50, activation: 'relu' }));
    model.add(tf.layers.dense({ units: 10, activation: 'sigmoid' }));


    model.add(tf.layers.dense({ units: 1, useBias: true }));

    return model;
}

function converter(data) {
    return tf.tidy(() => {
        tf.util.shuffle(data);
        const inputs = data.map(d => d.horsepower);
        const labels = data.map(d => d.mpg);

        const input_tensor = tf.tensor2d(inputs, [inputs.length, 1]);
        const label_tensor = tf.tensor2d(labels, [labels.length, 1]);

        const input_max = input_tensor.max();
        const input_min = input_tensor.min();
        const label_max = label_tensor.max();
        const label_min = label_tensor.min();

        const norm_inputs = input_tensor.sub(input_min).div(input_max.sub(input_min));
        const norm_labels = label_tensor.sub(label_min).div(label_max.sub(label_min));

        return {
            inputs: norm_inputs,
            labels: norm_labels,
            input_min, input_max, label_min, label_max
        }
    })
}

async function modelTrainer(model, inputs, labels) {
    model.compile({
        optimizer: tf.train.adam(),
        loss: tf.losses.meanSquaredError,
        metrics: ['mse']
    });

    const batchSize = 32;
    const epochs = 200;

    return await model.fit(inputs, labels, {
        batchSize,
        epochs,
        shuffle: true,
        callbacks: tfvis.show.fitCallbacks(
            { name: 'Training Preformance' },
            ['loss', 'mse'],
            {
                xLabel: 'loss',
                yLabel: 'mse',
                callbacks: ['onEpochEnd'],
                height: 500,
            }
        )
    });
}

function modelTester(model, inputData, norm_data) {

    const { input_min, input_max, label_min, label_max } = norm_data;

    const [xs, preds] = tf.tidy(() => {
        const xs = tf.linspace(0, 1, 100);
        const preds = model.predict(xs.reshape([100, 1]));

        const unNormXs = xs
            .mul(input_max.sub(input_min))
            .add(input_min);
        const unNormPreds = preds
            .mul(label_max.sub(label_min))
            .add(label_min);

        return [unNormXs.dataSync(), unNormPreds.dataSync()];
    });

    const predictedPoints = Array.from(xs).map((val, i) => {
        return { x: val, y: preds[i] }
    });

    const originalPoints = inputData.map(d => ({
        x: d.horsepower,
        y: d.mpg
    }));

    tfvis.render.scatterplot(
        { name: 'Model PRediction vs. Original DatA' },
        {
            values: [originalPoints, predictedPoints],
            series: ['original', 'predicted']
        },
        {
            xLabel: 'HorsePower',
            yLabel: 'MPG',
            height: 300,

        }
    );
}

async function run() {
    const data = await getData();
    const values = data.map(car => ({
        x: car.horsepower,
        y: car.mpg
    }));
    console.log(values);

    tfvis.render.scatterplot(
        { name: 'MPG vs. Horsepower' },
        { values: values },
        {
            xLabel: 'Horsepower',
            yLabel: 'MPG',
            height: 300,

        }
    )

    const model = modelCreator();
    tfvis.show.modelSummary({ name: 'Model Summary' }, model);

    const tensorData = converter(data);
    const { inputs, labels } = tensorData;
    await modelTrainer(model, inputs, labels);
    console.log("Done");

    modelTester(model, data, tensorData);
}

document.addEventListener('DOMContentLoaded', run);